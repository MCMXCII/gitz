Gitz is used to add another level of functionality to native git.

To get started you will need to create a file next to .gitz called .gitArray which will take the following format:

# Aliases
alias *GITNAME*='cd *GITLOCATION*'
alias *GITNAME*='cd *GITLOCATION*'
alias *GITNAME*='cd *GITLOCATION*'

This is used in the command line to navigate to a particular repo, and by Gitz.

# Array declaration
gitz=(
	"*GITNAME*"
	"*GITNAME*"
	"*GITNAME*"
)

This will include to a reference to all the repos you wish to include within Gitz.

Once this is setup, you need only to source .gitz in your bash_profile or bashrc to get going.